#!/bin/sh
set -e

# For the reflectances at the 5 angles compute by the PARASOL simulator.
# Mask out the missing points and combine into a single file with
# ibuf(4) corresponding to a different angle, i.e., PL01 -> PL, ibuf(4)=1, etc.

if [ $# -ne 7 ] ; then
  echo "Error in $0: invalid number of arguments $#"
  exit 1
fi
pfx=$1
pl01=$2
pl02=$3
pl03=$4
pl04=$5
pl05=$6
out=$7

files=""
n=1
while [ $n -le 5 ] ; do
  n=`echo $n | awk '{printf "%02d", $1}'`

  eval v=\$pl$n

  access $v ${pfx}_$v
  echo "C* FMSKPLT        -1 NEXT   GT     -999.    1     1.E38    1" | ccc fmskplt $v m$v

  # Relabel the name and ibuf(4)
  echo "C*RELABL   GRID
C*         GRID             PL$n" | ccc relabl m$v f$n

  release $v m$v

  files="$files f$n"
  n=`expr $n + 1`
done

# merge all files into one
rmerge ${pfx}_${out} $files

rm $files .ccc_cards
