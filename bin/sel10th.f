      PROGRAM SEL10TH
C     PROGRAM SEL10TH(IN,        OUT,            INPUT,          OUTPUT,)       B2
C    2          TAPE1=IN,  TAPE2=OUT,     TAPE5 =INPUT,   TAPE6 =OUTPUT)
C     -----------------------------------------------------------               B2
C                                                                               B2
C                                                                               B2
CSEL10TH - SELECT EVERY 10TH YEAR FROM MONTHLY FILES (YYYYMM).          1  1 C  B1
C                                                                               B3
CAUTHOR  - S. KHARIN.                                                           B3
C                                                                               B3
CPURPOSE - SELECT EVERY 10TH YEAR FROM MONTHLY FILES (YYYYMM).                  B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN = FILE CONTAINING INPUT FIELDS IN CCRN FORMAT WITH YYYYMM DATES.      B3
C                                                                               B3
CINPUT PARAMETERS...
C
C   IYEAR >0, FIRST YEAR TO SELECT FROM THE BEGINNING OF THE FILE.              B5
C             FOR EXAMPLE, IYEAR=1 WILL SELECT 1ST, 11TH, ETC. YEARS.           B5
C        <=0, SELECT YEARS ENDING ON |IYEAR|.                                   B5
C             FOR EXAMPLE, YEARS 185|IYEAR|,186|IYEAR|,187|IYEAR|,ETC.          B5
C
COUTPUT FILES...                                                                B3
C                                                                               B3
C     OUT = RECORDS FOR SELECTED YEARS                                          B3
C                                                                               B3
CEXAMPLES OF INPUT CARD...                                                      B5
C                                                                               B5
C*SEL10TH    -1                                                                 B5
C (SELECT YEARS ENDING ON 1)                                                    B5
C                                                                               B5
C*SEL10TH    10                                                                 B5
C (SELECT 10TH, 20TH, ETC. YEARS)                                               B5
C                                                                               B5
C*SEL10TH     0                                                                 B5
C (SELECT YEARS DIVISIBLE BY 10)                                                B5
C----------------------------------------------------------------------------
C
      LOGICAL OK,SKIP
C
      COMMON/ICOM/IBUF(8),IDAT(130682)
      CHARACTER*10 DATEFMT
C
      DATA MAXX/130682/
C---------------------------------------------------------------------
      NF=4
      CALL JCLPNT(NF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,END=900) IYEAR1                                               B4
C
C     * WRITE OUT THE INPUT PARAMETERS TO STANDARD OUTPUT
C
      WRITE(6,6010) IYEAR1
C
C     * READ NEXT RECORD
C
      NRIN=0
      NROUT=0
 100  CONTINUE
      CALL RECGET(1,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         IF(NRIN.EQ.0) CALL                        XIT('SEL10TH',-1)
         CALL PRTLAB(IBUF)
         WRITE(6,6020) NRIN  
         WRITE(6,6025) NROUT
         CALL                                      XIT('SEL10TH',0)
      ENDIF
C
C     * SKIP SUPERLABELS
C
      IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR"))GOTO 100
      NRIN=NRIN+1
      IF (NRIN.EQ.1) THEN
        CALL PRTLAB(IBUF)
C
C     * FIRST YEAR
C
        IYEAR0=IBUF(2)/100
      ENDIF
C
C     * CURRENT YEAR
C
      IYEAR=IBUF(2)/100
C
C     * WRITE OUT THE RECORD
C
      IF(IYEAR1.GT.0.AND.MOD(IYEAR-IYEAR0,10)+1.EQ.IYEAR1.OR.
     +   IYEAR1.LE.0.AND.MOD(IYEAR,10).EQ.IABS(IYEAR1))THEN
        CALL RECPUT(2,IBUF)
        NROUT=NROUT+1
      ENDIF
      GO TO 100

 900  CALL                                         XIT('SEL10TH',-2)
C---------------------------------------------------------------------
 5010 FORMAT (10X,I5)                                                           B4
 6010 FORMAT (' INPUT PARAMETERS:'/' YEAR1=',I5)
 6020 FORMAT (1X,I10,' RECORDS READ IN.')
 6025 FORMAT (1X,I10,' RECORDS WRITTEN OUT.')
      END
